﻿/// <reference path="../ExtJS/adapter/ext/ext-base.js" />
/// <reference path="../ExtJS/ext-all.js" />
/// <reference path="../ExtJS/ExtJSextend.js" />
/// <reference path="tabCommon.js" />

var grid = null;
var dataForm = null;
var dataWin = null;

//数据容器
var gridStore = new Ext.data.JsonStore({
    id: 'gridStore',
    fields: ['ID', 'FullName', 'Caption', 'Owner', 'IsDelete', 'CreateTime', 'UpdateTime', 'UpdateUser', 'Remark']
});

//加载grid的数据
var showcodebak = 1;
function LoadData(showCode) {
    if (showCode)
        showcodebak = showCode;

    var url = "";
    if (showcodebak == 1)
        url = "?IsAll=0&IsDelete=0"; //默认皆为0
    else if (showcodebak == 2)
        url = "?IsAll=0&IsDelete=1";
    else if (showcodebak == 3)
        url = "?IsAll=0";
    else if (showcodebak == 4)
        url = "?IsAll=1&IsDelete=0";
    else if (showcodebak == 5)
        url = "?IsAll=1&IsDelete=1";
    else if (showcodebak == 6)
        url = "?IsAll=1";

    tabAjaxPost('/Ajax/ChartList' + url, function (json) {
        if (json.code == 1) {
            gridStore.loadData(json.data);
        } else {
            errorMessage(json.msg);
        }
    });
}

Ext.onReady(function () {

    //grid 行展开显示内容
    var gridExpander = new Ext.ux.grid.RowExpander({
        tpl: new Ext.Template(
            '<p class="textselect">' + GetRowExpanderNbsp() + '<b>标识：</b>　{ID}</p>',
            '<p>' + GetRowExpanderNbsp() + '<b>名称：</b>　{FullName}</p>',
            '<p>' + GetRowExpanderNbsp() + '<b>介绍：</b>　{Caption}</p>',
            '<p>' + GetRowExpanderNbsp() + '<b>备注：</b>　{Remark}</p>'
            )
    });

    //显示数据表格
    grid = new Ext.grid.GridPanel({
        id: 'grid',
        scriptRow: true,
        store: gridStore,
        viewConfig: {
            firceFit: true,
            deferEmptyText: false,
            emptyText: '暂无流程图数据！',
            getRowClass: function (record, index) {
                if (!(index % 2)) {
                    return 'x-grid3-row';
                } else {
                    return 'x-grid3-row-alt';
                }
            }
        },
        columns: [
            new Ext.grid.RowNumberer(),
            gridExpander,
            { header: '标识', width: 260, dataIndex: 'ID', sortable: true, align: 'center', hidden: true },
            { header: '名称', width: 150, dataIndex: 'FullName', sortable: true, align: 'center' },
            { header: '介绍', width: 200, dataIndex: 'Caption', sortable: true, align: 'center' },
            { header: '所有者', width: 100, dataIndex: 'Owner', sortable: true, align: 'center', renderer: GetUserDiaplayName },
            { header: '创建时间', width: 180, dataIndex: 'CreateTime', sortable: true, align: 'center' },
            { header: '修改时间', width: 180, dataIndex: 'UpdateTime', sortable: true, align: 'center' },
            { header: '修改人', width: 100, dataIndex: 'UpdateUser', sortable: true, align: 'center', renderer: GetUserDiaplayName },
            { header: '已删除', width: 60, dataIndex: 'IsDelete', sortable: true, align: 'center', renderer: SetGridBoolValue },
            { header: '备注', width: 400, dataIndex: 'Remark', sortable: true, align: 'center', hidden: true }
        ],
        plugins: gridExpander,
        sm: new Ext.grid.RowSelectionModel({ singleSelect: true }),
        listeners: {
            'rowdblclick': function (grid, rowIndex, e) {
                EditData(rowIndex);
            },
            'rowcontextmenu': function (grid, rowIndex, e) {
                e.preventDefault();
                var grid_menus = new Ext.menu.Menu({
                    shadow: 'sides',
                    items: [{
                        text: '编辑',
                        iconCls: 'icons-chart-edit',
                        handler: function () {
                            EditData(rowIndex);
                        }
                    }, {
                        text: '绘图',
                        iconCls: 'icons-chartdiagram',
                        handler: function () {
                            CanvasData(rowIndex);
                        }
                    }, {
                        text: '授权他人使用',
                        iconCls: 'icons-authorization',
                        handler: function () {
                            AuthorizationUserData(rowIndex);
                        }
                    }, {
                        text: '删除/恢复',
                        iconCls: 'icons-chart-delete',
                        handler: function () {
                            DeleteData(rowIndex);
                        }
                    }]
                });
                grid_menus.showAt(e.getPoint());
            }
        }
    });

    //视图内容与布局
    var _center_tbar = [{
        text: '新增',
        iconCls: 'icons-chart-add',
        handler: function () {
            AddData();
        }
    }, {
        text: '编辑',
        iconCls: 'icons-chart-edit',
        handler: function () {
            EditData();
        }
    }, {
        text: '绘图',
        iconCls: 'icons-chartdiagram',
        handler: function () {
            CanvasData();
        }
    }, {
        text: '授权他人使用',
        iconCls: 'icons-authorization',
        handler: function () {
            AuthorizationUserData();
        }
    }, {
        text: '删除/恢复',
        iconCls: 'icons-chart-delete',
        handler: function () {
            DeleteData();
        }
    }, {
        text: '刷新有效数据',
        iconCls: 'icons-chart-search',
        handler: function () {
            LoadData(1);
        }
    }, {
        text: '显示删除数据',
        iconCls: 'icons-chart-search',
        handler: function () {
            LoadData(2);
        }
    }, {
        text: '显示全部数据',
        iconCls: 'icons-chart-search',
        handler: function () {
            LoadData(3);
        }
    }];
    if (parent.UserInfo().UserName == CacheData_sysadmin) {
        _center_tbar.push('->', {
            text: '显示所有用户有效数据',
            iconCls: 'icons-chart-search',
            handler: function () {
                LoadData(4);
            }
        }, {
            text: '显示所有用户删除数据',
            iconCls: 'icons-chart-search',
            handler: function () {
                LoadData(5);
            }
        }, {
            text: '显示所有用户全部数据',
            iconCls: 'icons-chart-search',
            handler: function () {
                LoadData(6);
            }
        });
    }
    var _center = new Ext.Panel({
        region: 'center',
        layout: 'fit',
        items: [grid],
        tbar: _center_tbar
    });
    var _view = new Ext.Viewport({
        layout: 'border',
        items: [_center]
    });

    //加载部门与用户
    _depLoadData();
    _userLoadData();

    //加载数据
    LoadData(1);

    //创建表单
    CreateForm();

    //若加载信息中有图形则打开绘图
    if(parent.LoadInfo.chartid != "") {
        var rowCount = gridStore.getCount();
        for (var i = 0; i < rowCount; i++) {
            if (gridStore.getAt(i).get("ID") == parent.LoadInfo.chartid) {
                //选中默认行
                grid.getSelectionModel().selectRow(i, true);
                CanvasData();
                break;
            }
        }
    }

});

//新增数据
function AddData() {
    dataForm.getForm().reset();
    dataWin.setTitle("新建流程图信息");

    //选中指定用户的部门后加载部门用户并选中该用户
    Load_dataForm_Owner_ForUserID(GetLoginUser().ID);

    dataWin.show();
}

//编辑行数据
function EditData(rowIndex) {
    var row = GetSelectionRow('grid', rowIndex);
    if (row) {
        dataWin.setTitle("编辑流程图信息 - " + row.json.FullName);
        
        //选中指定用户的部门后加载部门用户并选中该用户
        Load_dataForm_Owner_ForUserID(row.json.Owner.toString().toLowerCase());

        dataWin.show();
        dataForm.getForm().loadRecord(row);
    }
    else {
        errorMessage("请先点选一行数据！");
    }
}

//前去绘图
function CanvasData(rowIndex) {
    var row = GetSelectionRow('grid', rowIndex);
    if (row) {
        OpenTabCanvas(row.json.ID, row.json.FullName);
    }
    else {
        errorMessage("请先点选一行数据！");
    }
}

//前去授权他人使用
function AuthorizationUserData(rowIndex)
{
    var row = GetSelectionRow('grid', rowIndex);
    if (row) {
        OpenTabAuthorizationUser(row.json.ID, row.json.FullName);
    }
    else {
        errorMessage("请先点选一行数据！");
    }
}

//删除行数据
function DeleteData(rowIndex) {
    var row = GetSelectionRow('grid', rowIndex);
    if (row) {
        Ext.Msg.confirm('温馨提示', '您确定要删除/恢复该信息吗？', function (btn) {
            if (btn == 'yes') {
                tabAjaxPostData('/Ajax/ChartDelete', { ID: row.json.ID }, function (json) {
                    if (json.code == 1) {
                        LoadData();
                    } else {
                        errorMessage(json.msg);
                    }
                });
            }
        });
    }
    else {
        errorMessage("请先点选一行数据！");
    }
}


//用于表单中按部门分配用户
var m_userStore = new Ext.data.JsonStore({
    id: 'm_userStore',
    fields: ['ID', 'UserName', 'DepartmentID', 'FullName', 'DisplayName']
});
//选中指定用户的部门后加载部门用户并选中该用户
function Load_dataForm_Owner_ForUserID(userID) {
    for (var i = 0; i < _userStore.getCount() ; i++) {
        if (_userStore.getAt(i).json.ID.toString().toLowerCase() == userID) {
            var depID = _userStore.getAt(i).json.DepartmentID.toString();
            Ext.getCmp('dataForm_DepartmentID').setValue(depID);
            Load_m_userStore(depID);
            Ext.getCmp('dataForm_Owner').setValue(userID);
            break;
        }
    }
}
//加载表单中指定部门的用户
function Load_m_userStore(depID)
{
    Ext.getCmp('dataForm_Owner').clearValue();
    m_userStore.removeAll();
    for (var i = 0; i < _userStore.getCount() ; i++) {
        if (_userStore.getAt(i).json.DepartmentID.toString().toLowerCase() == depID.toString().toLowerCase())
            m_userStore.add(_userStore.getAt(i));
    }
}

//创建表单
function CreateForm() {
    //使用表单提示
    Ext.QuickTips.init();
    Ext.form.Field.prototype.msgTarget = 'side';

    dataForm = new Ext.FormPanel({
        labelWidth: 80,
        labelAlign: 'right',
        border: false,
        defaultType: 'textfield',
        buttomAlign: 'center',
        bodyStyle: 'padding: 15px',
        items: [{
            inputType: 'hidden',
            name: 'ID'
        }, {
            fieldLabel: '名称',
            name: 'FullName',
            anchor: '90%',
            allowBlank: false,
            emptyText: '请填写流程图名称',
            blankText: '流程图名称不能为空'
        }, {
            xtype: 'textarea',
            fieldLabel: '介绍',
            name: 'Caption',
            anchor: '90%',
            emptyText: '可选填介绍内容',
            maxLength: 400
        }, new Ext.form.ComboBox({
            id: 'dataForm_DepartmentID',
            fieldLabel: '所属用户部门',
            name: 'DepartmentID',
            hiddenName: 'DepartmentID',
            anchor: '90%',
            allowBlank: false,
            emptyText: '请选择所属用户部门',
            blankText: '所属用户部门不能为空',
            triggerAction: 'all', //默认仅显示符合输入的项
            store: _depStore,
            displayField: 'FullName',
            valueField: 'ID',
            mode: 'local',
            listeners: {
                'select': function (combo, record, index) {
                    Load_m_userStore(record.json.ID);
                }
            }
        }), new Ext.form.ComboBox({
            id: 'dataForm_Owner',
            fieldLabel: '所属用户',
            name: 'Owner',
            hiddenName: 'Owner',
            anchor: '90%',
            allowBlank: false,
            emptyText: '请选择所属用户',
            blankText: '所属用户不能为空',
            triggerAction: 'all', //默认仅显示符合输入的项
            store: m_userStore,
            displayField: 'DisplayName',
            tpl: '<tpl for="."><div class="x-combo-list-item">姓名：{FullName}；用户名：{UserName}</div></tpl>',
            valueField: 'ID',
            mode: 'local'
        }), new Ext.form.Checkbox({
            name: 'IsDelete',
            fieldLabel: '删除',
            boxLabel: '勾选则该流程图信息被删除！'
        }), {
            xtype: 'textarea',
            fieldLabel: '备注',
            name: 'Remark',
            anchor: '90%',
            emptyText: '可选填备注内容',
            maxLength: 400
        }],
        buttons: [{
            text: '提交',
            iconCls: 'icons-base-accept',
            handler: function () {
                SaveData();
            }
        }, {
            text: '退出',
            iconCls: 'icons-base-cancel',
            handler: function () {
                dataWin.hide();
            }
        }]
    });

    //表单显示的窗体
    dataWin = new Ext.Window({
        title: '流程图信息',
        layout: 'fit',
        width: 400,
        height: 350,
        closeAction: 'hide',
        iconCls: 'icons-chart',
        plain: true,
        modal: true,
        maskDisabled: true,
        items: dataForm
    });

}

//提交编辑
function SaveData() {
    tabFormSubmit(dataForm, '/Ajax/ChartEdit', function (json) {
        if (json.code == 1) {
            alertMessage("操作成功！");
            dataWin.hide();
            LoadData();
        }
        else
            errorMessage(json.msg);
    });
}
