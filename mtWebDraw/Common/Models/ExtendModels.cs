﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mtWebDraw.Common
{
    /// <summary>
    /// 更新流程图授权码类
    /// </summary>
    public class ChartCode_Update
    {

        /// <summary>
        /// 用户ID
        /// </summary>
        public Guid ID { get; set; }

        /// <summary>
        /// 授权码
        /// </summary>
        public int Code { get; set; }

    }
}